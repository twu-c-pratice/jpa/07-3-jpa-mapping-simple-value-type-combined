package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class CompanyProfile {

    @Id @GeneratedValue private Long id;

    @Embedded private Address address;

    public CompanyProfile() {}

    public CompanyProfile(Address address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public Address getAddress() {
        return address;
    }
}
