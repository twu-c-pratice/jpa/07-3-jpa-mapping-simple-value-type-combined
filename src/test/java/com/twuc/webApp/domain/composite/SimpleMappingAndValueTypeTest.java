package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jws.soap.SOAPBinding;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTypeTest extends JpaTestBase {

    @Autowired CompanyProfileRepository cpRepo;
    @Autowired UserProfileRepository upRepo;

    @Test
    void should_save_and_get_company_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(
                em -> {
                    final CompanyProfile companyProfile =
                            cpRepo.save(new CompanyProfile(new Address("Xi'an", "ZhangBaBei")));
                    expectedId.setValue(companyProfile.getId());
                });

        run(
                em -> {
                    final CompanyProfile savedCompanyProfile = cpRepo.getOne(expectedId.getValue());

                    assertEquals("Xi'an", savedCompanyProfile.getAddress().getCity());
                    assertEquals("ZhangBaBei", savedCompanyProfile.getAddress().getStreet());
                });
    }

    @Test
    void should_save_and_get_user_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(
                em -> {
                    final UserProfile userProfile =
                            upRepo.save(new UserProfile(new Address("Xi'an", "XiaoZhai")));
                    expectedId.setValue(userProfile.getId());
                });

        run(
                em -> {
                    final UserProfile savedUserProfile = upRepo.getOne(expectedId.getValue());

                    assertEquals("Xi'an", savedUserProfile.getAddress().getCity());
                    assertEquals("XiaoZhai", savedUserProfile.getAddress().getStreet());
                });
    }
}
